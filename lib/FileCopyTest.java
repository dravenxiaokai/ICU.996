package com.imooc.zhangxiaoxi.resource;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Project 996
 * @ClassName FileCopyTest
 * @Description JDK7之前文件拷贝功能
 * @Author pankai@cnsesan.com
 * @CreateTime 2019/11/27 19:07
 * @Modifier
 * @ModifyTime 2019/11/27 19:07
 * @ModifyNote to describe what you modified
 */
public class FileCopyTest {
    @Test
    public void copyFile() {
        /**
         * 创建输入/输出流
         * 执行文件拷贝，读取文件内容，写入到另一个文件中
         * 关闭文件流资源
         */
        // 定义输入路径和输出路径
        String originalUrl = "lib/FileCopyTest.java";
        String targetUrl = "targetTest/target.txt";

        // 声明文件输入流，文件输出流
        FileInputStream originalFileInputStream = null;
        FileOutputStream targetFileOutputStream = null;

        try {
            // 实例化文件流对象
            originalFileInputStream = new FileInputStream(originalUrl);

            targetFileOutputStream = new FileOutputStream(targetUrl);

            // 读取的字节信息
            int content;

            // 迭代，读取写入字节
            while ((content = originalFileInputStream.read()) != -1) {
                targetFileOutputStream.write(content);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭流资源
            if (targetFileOutputStream != null) {
                try {
                    targetFileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (originalFileInputStream != null) {
                try {
                    originalFileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
