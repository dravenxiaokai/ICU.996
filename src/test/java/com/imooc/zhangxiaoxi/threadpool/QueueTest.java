package com.imooc.zhangxiaoxi.threadpool;

import org.junit.Test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;

/**
 * @Project 996
 * @ClassName QueueTest
 * @Description TODO
 * @Author pankai@cnsesan.com
 * @CreateTime 2019/11/27 23:09
 * @Modifier
 * @ModifyTime 2019/11/27 23:09
 * @ModifyNote to describe what you modified
 */
public class QueueTest {
    @Test
    public void arrayBlockingQueue() throws InterruptedException {
        /**
         * 有界队列，队列容量为10
         */
        ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(10);

        // 循环向对列中添加元素
        for (int i = 0; i < 20; i++) {
            queue.put(i);
            System.out.println("向队列中添加值：" + i);
        }
    }

    @Test
    public void linkedBlockingQueue() throws InterruptedException {
        /**
         * 基于链表的有界/无界阻塞队列，队列容量为10
         */
        LinkedBlockingQueue queue =
                new LinkedBlockingQueue<Integer>();
        // 循环向对列中添加元素
        for (int i = 0; i < 20; i++) {
            queue.put(i);
            System.out.println("向队列中添加值：" + i);
        }
    }

    @Test
    public void synchronousQueue() throws InterruptedException {
        /**
         * 同步移交阻塞队列
         */
        SynchronousQueue<Integer> queue = new SynchronousQueue<>();

        // 插入值
        new Thread(()->{
            try {
                queue.put(1);
                System.out.println("插入成功");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        // 删除值
//        new Thread(()->{
//            try {
//                queue.take();
//                System.out.println("删除成功");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }).start();

        Thread.sleep(1000L*60);
    }
}
