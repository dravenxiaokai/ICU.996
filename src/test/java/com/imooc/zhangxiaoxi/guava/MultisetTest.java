package com.imooc.zhangxiaoxi.guava;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.primitives.Chars;
import org.junit.Test;

/**
 * @Project 996
 * @ClassName MultisetTest
 * @Description 实现：使用Multiset统计一古诗的文字出现的频率
 * @Author pankai@cnsesan.com
 * @CreateTime 2019/11/27 20:34
 * @Modifier
 * @ModifyTime 2019/11/27 20:34
 * @ModifyNote to describe what you modified
 */
public class MultisetTest {
    private static final String text =
            "南陵别儿童入京\n" +
                    "白酒新熟山中归，黄鸡啄黍秋正肥。\n" +
                    "呼童烹鸡酌白酒，儿女嬉笑牵人衣。\n" +
                    "高歌取醉欲自慰，起舞落日争光辉。\n" +
                    "游说万乘苦不早，著鞭跨马涉远道。\n" +
                    "会稽愚妇轻买臣，余亦辞家西入秦。\n" +
                    "仰天大笑出门去，我辈岂是蓬蒿人。";

    @Test
    public void handle() {
        // multiset创建
        Multiset<Character> multiset = HashMultiset.create();

        // string 转换成 char 数组
        char[] chars = text.toCharArray();

        // 遍历数组，添加到multiset中
        Chars.asList(chars)
                .stream()
                .forEach(charItem -> {
                    multiset.add(charItem);
                });

        System.out.println("size: " + multiset.size());

        System.out.println("count: " + multiset.count('人'));
    }
}
