package com.imooc.zhangxiaoxi.validation;

import javax.validation.Valid;

/**
 * @Project 996
 * @ClassName UserInfoService
 * @Description 用户信息服务类
 * @Author pankai@cnsesan.com
 * @CreateTime 2019/11/28 9:04
 * @Modifier
 * @ModifyTime 2019/11/28 9:04
 * @ModifyNote to describe what you modified
 */
public class UserInfoService {

    /**
     * UserInfo 作为输入参数
     *
     * @param userInfo
     */
    public void setUserInfo(@Valid UserInfo userInfo) {
    }

    ;

    /**
     * UserInfo 作为输出参数
     *
     * @return
     */
    public @Valid UserInfo getUserInfo() {
        return new UserInfo();
    }

    /**
     * 默认构造函数
     */
    public UserInfoService() {

    }

    /**
     * 接受UserInfo作为参数的构造函数
     */
    public UserInfoService(@Valid UserInfo userInfo) {

    }

}
