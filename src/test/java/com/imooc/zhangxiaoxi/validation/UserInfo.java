package com.imooc.zhangxiaoxi.validation;

import org.hibernate.validator.constraints.Length;

import javax.validation.GroupSequence;
import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.validation.groups.Default;
import java.util.Date;
import java.util.List;

/**
 * @Project 996
 * @ClassName UserInfo
 * @Description 等待验证对象实体类
 * 用户信息类
 * @Author pankai@cnsesan.com
 * @CreateTime 2019/11/28 0:10
 * @Modifier
 * @ModifyTime 2019/11/28 0:10
 * @ModifyNote to describe what you modified
 */
public class UserInfo {

    // 登录场景
    public interface LoginGroup {
    }

    // 注册场景
    public interface RegisterGroup {
    }

    // 组排序场景
    @GroupSequence({
            LoginGroup.class,
            RegisterGroup.class,
            Default.class
    })
    public interface Group {}

    /**
     * 用户ID，登录时不能为空
     */
    @NotNull(message = "用户ID不能为空", groups = LoginGroup.class)
    private String userId;

    /**
     * NotEmpty不会自动去掉前后空格
     */
    @NotEmpty(message = "用户名不能为空")
    private String userName;

    /**
     * NotBlank 自动去掉字符串前后空格后验证是否为空
     */
    @NotBlank(message = "用户密码不能为空")
    @Length(min = 6, max = 20, message = "密码不能少于6位，不能多于20位")
    private String passWord;

    /**
     * 邮箱，注册时必填
     */
    @NotNull(message = "邮箱不能为空", groups = RegisterGroup.class)
    @Email(message = "邮箱必须为有效邮箱")
    private String email;

    @Phone(message = "手机号不是158后头随便")
    private String phone;

    @Min(value = 18, message = "年龄不能小于18岁")
    @Max(value = 60, message = "年龄不能大于60岁")
    private Integer age;

    @Past(message = "生日不能为未来时间点")
    private Date birthday;

    /**
     * 好友列表
     * Valid 级联验证
     */
    @Size(min = 1, message = "不能少于1个好友")
    private List<@Valid UserInfo> friends;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<UserInfo> getFriends() {
        return friends;
    }

    public void setFriends(List<UserInfo> friends) {
        this.friends = friends;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
