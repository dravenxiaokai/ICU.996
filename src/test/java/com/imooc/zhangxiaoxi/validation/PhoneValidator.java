package com.imooc.zhangxiaoxi.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Project 996
 * @ClassName PhoneValidator
 * @Description TODO
 * @Author pankai@cnsesan.com
 * @CreateTime 2019/11/28 10:04
 * @Modifier
 * @ModifyTime 2019/11/28 10:04
 * @ModifyNote to describe what you modified
 */
public class PhoneValidator implements ConstraintValidator<Phone, String> {

    /**
     * 自定义校验逻辑方法
     *
     * @param value
     * @param context
     * @return
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // 手机号验证规则：158后头随便
        String check = "158\\d{8}";
        Pattern regex = Pattern.compile(check);
        // 空值处理
        String phone = Optional.ofNullable(value).orElse("");
        Matcher matcher = regex.matcher(phone);

        return matcher.matches();
    }
}
